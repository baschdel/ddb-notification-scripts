# DDB notification scripts

These scripts are for getting notifications stored in a [DDB Server](https://gitlab.com/baschdel/ddb).

You need the ddb_connector.sh script for these scripts to work as they only read and write to their stdio. I recommend you to put it in a directory your PATH variable point to. (Don forget to chmod +x)

## dunst_notification_serializer.lua

This script is used by the dunst_notification_hook.sh script to turn the envoirenment variables into commands for the ddb server to add the noificaion object and a signal for the notification broker.

You can put this block into your dunstrc to use it.
NOTE: I placed both the dunst_notification_hook.sh and dunst_notification_serializer.lua files into the ~/.scripts folder.

```
[ddb_notify]
	script="~/.scripts/dunst_notification_hook.sh"
```

## notification_broker.lua

This script manages the notification lists that are used by frontends that display notifications so that they don't have to build the lits themselves.

Usage Example:
```
ddb_connector.sh localhost 5678 test "lua notification_broker.lua exni"
```
