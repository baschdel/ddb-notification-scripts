--[[
	This script serves as a manager for notifications so that other scripts don't have to mess wih requesting and updating the notification lists, restarting this script will result in all notifications being removed from the list when it comes back up (they are still in the key value store though, so you could readd them), but for now it is best to not restart it.
]]--

local broker_proxy_name = ...
local notification_names = {}

local function subscribe(name)
	print("+ "..name)
end

local function unsubscribe(name)
	print("- "..name)
end

local function update_notification_lists()
	local active_list_command = "> "..broker_proxy_name.." active_list"
	local inactive_list_command = "> "..broker_proxy_name.." inactive_list"
	local active_list_empty = true
	local inactive_list_empty = true
	for name,is_not_hidden in pairs(notification_names) do
		if is_not_hidden then
			active_list_command = active_list_command.." "..name
			active_list_empty = false
		else
			inactive_list_command = inactive_list_command.." "..name
			inactive_list_empty = false
		end
	end
	if active_list_empty then
		active_list_command = active_list_command.." "
	end
	if inactive_list_empty then
		inactive_list_command = inactive_list_command.." "
	end
	print(active_list_command)
	print(inactive_list_command)
end

print("> "..broker_proxy_name.." type notification_broker")
subscribe(broker_proxy_name)
update_notification_lists()

function handle_input(i)
	local new_notification = i:match("^s "..broker_proxy_name.." notify:([^ ]+)$")
	if new_notification then
		notification_names[new_notification] = true
		update_notification_lists()
		subscribe(new_notification)
		print("r "..new_notification.." is_hidden")
		return
	end
	local deleted_notification = i:match("^u (.-) type$")
	if deleted_notification == broker_proxy_name then
		os.exit()
	end
	if deleted_notification then
		unsubscribe(deleted_notification)
		notification_names[deleted_notification] = nil
		update_notification_lists()
	end
	local notification,is_hidden = i:match("^> (.-) is_hidden (.*)$")
	if notification then
		notification_names[notification] = is_hidden ~= "true"
		update_notification_lists()
	end
end

while true do
	local i = io.read()
	handle_input(i)
end
